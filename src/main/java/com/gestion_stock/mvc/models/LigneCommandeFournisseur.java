package com.gestion_stock.mvc.models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeFournisseur implements Serializable {

    @Id
    @GeneratedValue
    private Long idCdeFrs;
    
    @ManyToOne
    @JoinColumn (name = "idarticle")
    private Article article;
    
    @ManyToOne
    @JoinColumn (name = "idcommandeFournisseur")
    private CommandeFournisseur commandeFournisseur;

    public LigneCommandeFournisseur() {
    }

    public Long getIdCdeFrs() {
        return idCdeFrs;
    }

    public void setIdCdeFrs(Long idCdeFrs) {
        this.idCdeFrs = idCdeFrs;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public CommandeFournisseur getCommandeFournisseur() {
        return commandeFournisseur;
    }

    public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
        this.commandeFournisseur = commandeFournisseur;
    }

    
}
